const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const getClientEnvironment = require('./env');
const paths = require('./paths');

const publicPath = '/';
const publicUrl = '';
const env = getClientEnvironment(publicUrl);

module.exports = {
  devtool: 'source-map',
  entry: [
    'react-hot-loader/patch',
    require.resolve('./polyfills'),
    require.resolve('react-dev-utils/webpackHotDevClient'),
    paths.appIndexJs,
  ],
  output: {
    path: paths.appBuild,
    pathinfo: true,
    filename: 'static/js/bundle.js',
    chunkFilename: 'static/js/[name].chunk.js',
    publicPath,
    devtoolModuleFilenameTemplate: (info) =>
      path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'),
  },
  resolve: {
    modules: ['node_modules', paths.appNodeModules, 'src'].concat(
      process.env.NODE_PATH.split(path.delimiter).filter(Boolean),
    ),
    extensions: ['.web.js', '.mjs', '.js', '.json', '.web.jsx', '.jsx'],
    plugins: [new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson])],
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        oneOf: [
          {
            test: /\.(bmp|gif|jpg|jpeg|png|svg)$/,
            oneOf: [
              {
                issuer: /\.css$/,
                oneOf: [
                  {
                    test: /\.svg$/,
                    loader: 'svg-url-loader',
                    options: {
                      name: 'static/media[name].[ext]?[hash:8]',
                      limit: 8192,
                    },
                  },
                  {
                    loader: 'url-loader',
                    options: {
                      name: 'static/media/[name].[ext]?[hash:8]',
                      limit: 8192,
                    },
                  },
                ],
              },
              {
                loader: 'file-loader',
                options: {
                  name: 'static/media/[name].[ext]?[hash:8]',
                },
              },
            ],
          },
          {
            test: /\.(js|jsx)$/,
            include: paths.appSrc,
            loader: require.resolve('babel-loader'),
            options: {
              cacheDirectory: true,
            },
          },
          {
            test: /\.css$/,
            use: [
              require.resolve('style-loader'),
              {
                loader: require.resolve('css-loader'),
                options: {
                  importLoaders: 1,
                },
              },
              {
                loader: require.resolve('postcss-loader'),
                options: {
                  config: {
                    path: './config/postcss.config.js',
                  },
                },
              },
            ],
          },
          {
            test: /\.txt$/,
            loader: 'raw-loader',
          },
          {
            exclude: [
              /\.(jsx|js)$/,
              /\.css$/,
              /\.html$/,
              /\.(bmp|gif|jpg|jpeg|png|svg)$/,
              /\.json$/,
              /\.txt$/,
            ],
            loader: 'file-loader',
            options: {
              name: 'static/media/[name].[ext]?[hash:8]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new ProgressBarPlugin({
      format: `  build [:bar] ${chalk.green.bold(':percent')}${chalk.blue(
        ' :msg',
      )} (:elapsed seconds)`,
      clear: false,
    }),
    new InterpolateHtmlPlugin(env.raw),
    new HtmlWebpackPlugin({
      chunkSortMode: 'dependency',
      inject: true,
      template: paths.appHtml,
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin(env.stringified),
    new webpack.HotModuleReplacementPlugin(),
    new CaseSensitivePathsPlugin(),
    new WebpackNotifierPlugin({ alwaysNotify: true }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'server',
      analyzerHost: '127.0.0.1',
      analyzerPort: 3001,
      reportFilename: 'report.html',
      defaultSizes: 'stat',
      openAnalyzer: false,
      generateStatsFile: false,
      statsFilename: 'stats.json',
      statsOptions: null,
      logLevel: 'info',
    }),
    new WatchMissingNodeModulesPlugin(paths.appNodeModules),
  ],
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty', // eslint-disable-line camelcase
  },
  performance: {
    hints: false,
  },
};
