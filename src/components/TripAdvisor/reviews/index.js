import React, { Component } from 'react';
import ReviewsCircleComponent from './circle';

import './style.css';

const ReviewsComponent = ({count, rating}) => {
  return (
    <div className="reviews_count"> 
      {
        new Array(count).fill(0).map((nan, i) => (
          <ReviewsCircleComponent key={i} circleNum={i+1} rating={rating} />)
        )
      }
    </div>
  );
};

export default ReviewsComponent;