import React, { Component } from 'react';
import CountUp from 'react-countup';

const ReviewsCircleComponent = ({circleNum, rating}) => {
  let circleClass = '';
  if (rating >= circleNum) circleClass = 'full';
  if (((circleNum - rating) > 0) && ((circleNum - rating) < 1)) circleClass = 'half';
  return (
      <div className={`reviews_circle ${circleClass}`}></div>
  );
};

export default ReviewsCircleComponent;