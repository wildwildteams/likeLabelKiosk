import React, { Component } from 'react';
import ReviewsComponent from './reviews';
import GooglQR from '../../utils/GoogleQR';

import './style.css';

class TripAdvisorWall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      name: '',
    };
    this.tripAdvisorTimer = undefined;
  }

  componentDidMount() {
    this.startTimerTripAdviser(
      {
        social: 'tripadvisor',
        link:
          'https://www.tripadvisor.de/Restaurant_Review-g187323-d4472820-Reviews-Cafe_Chagall-Berlin.html',
        serviceToken: '87934974ffcce31fc8b4c276da851ccd306bf401',
      },
      1000,
    );
  }

  componentWillUnmount() {
    clearTimeout(this.tripAdvisorTimer);
  }

  startTimerTripAdviser(req, delay) {
    this.tripAdvisorTimer = setTimeout(() => {
      this.updateSocialInfo(req);
    }, delay);
  }

  updateSocialInfo({ social, link, serviceToken }) {
    fetch(
      `https://boiling-brushlands-33834.herokuapp.com/api/v1/data/tripadvisor/profile?link=${link}&service_token=${serviceToken}&_=${new Date().getTime()}`,
      { cache: 'no-store' },
    )
      .then((response) => response.json())
      .then((response) => {
        if (
          this.state.rating !== response.result.rating ||
          this.state.name !== response.result.name
        ) {
          this.setState({
            rating: response.result.rating,
            name: response.result.name,
          });
        }
        return true;
      })
      .then(() =>
        this.startTimerTripAdviser({ social, link, serviceToken }, 30000),
      )
      .catch((error) => {
        console.log(error);
        this.startTimerTripAdviser({ social, link, serviceToken }, 30000);
      });
  }
  render() {
    return (
      <div className="wrapper tripadvisor">
        <div className="wrapper_inner">
          <h1 className="title">rate us &quot;{this.state.name}&quot;</h1>
          <h2 className="title_sub">Rating: {this.state.rating}</h2>
          <div className="countainers">
            <div className="profile">
              <img
                className="profile_img"
                src="img/tripadvisor-wisi-logo.png"
                alt=""
              />
            </div>
            <div className="reviews">
              <ReviewsComponent rating={this.state.rating} count={5} />
            </div>
          </div>
          <div className="contact_us">
            <img
              className="contact_us_img"
              src="img/tripadvisor-text-logo.png"
              alt=""
            />
            <GooglQR
              url="https://www.tripadvisor.de/Restaurant_Review-g187323-d4472820-Reviews-Cafe_Chagall-Berlin.html"
              style={{ width: '100%' }}
              level="M"
              margin={1}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default TripAdvisorWall;
