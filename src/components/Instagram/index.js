import React, { Component } from 'react';
import CountUp from 'react-countup';

import GooglQR from '../../utils/GoogleQR';

import './style.css';

class InstagramWall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      instaFollowers: 0,
      instaLogin: '',
      newInstaFollowers: 0,
    };
    this.instaTimer = undefined;
  }

  componentDidMount() {
    this.startTimerInsta(
      {
        social: 'instagram',
        profileId: '4351583419',
        serviceToken: '87934974ffcce31fc8b4c276da851ccd306bf401',
      },
      1000 + Math.floor(Math.random() * 11),
    );
  }

  componentWillUnmount() {
    clearTimeout(this.instaTimer);
  }

  startTimerInsta(req, delay) {
    this.instaTimer = setTimeout(() => {
      this.updateSocialInfo(req);
    }, delay);
  }

  updateSocialInfo({ social, profileId, link, serviceToken }) {
    fetch(
      `https://boiling-brushlands-33834.herokuapp.com/api/v1/data/instagram/profile?id=${profileId}&service_token=${serviceToken}&_=${new Date().getTime()}`,
      { cache: 'no-store' },
    )
      .then((response) => response.json())
      .then((response) => {
        if (
          response.followers - this.state.newInstaFollowers >= 1 ||
          this.state.newInstaFollowers - response.followers >= 2
        ) {
          this.setState((prevState) => ({
            instaLogin: response.username,
            instaFollowers: prevState.newInstaFollowers,
            newInstaFollowers: response.followers,
            link,
          }));
        }
      })
      .then(() =>
        this.startTimerInsta(
          { social, profileId, serviceToken },
          20000 + Math.floor(Math.random() * 11),
        ),
      )
      .catch((error) => {
        console.log(error);
        this.startTimerInsta(
          { social, profileId, serviceToken },
          20000 + Math.floor(Math.random() * 11),
        );
      });
  }

  render() {
    return (
      <div className="wrapper instagram">
        <div className="wrapper_inner">
          <h1 className="title">follow us</h1>
          <div className="countainers">
            <div className="profile">
              <img
                className="profile_img"
                src="img/insta-img-logo.png"
                alt=""
              />
              <span className="profile_name">@{this.state.instaLogin}</span>
            </div>
            <div className="followers">
              <div className="followers_count">
                <CountUp
                  start={this.state.instaFollowers}
                  end={this.state.newInstaFollowers}
                />
              </div>
              <div className="followers_text">followers</div>
            </div>
          </div>
          <div className="contact_us">
            <img
              className="contact_us_img"
              src="img/insta-text-logo.png"
              alt=""
            />
            <GooglQR
              url="https://www.instagram.com/chagall_kollwitzstrasse/"
              style={{ width: '100%' }}
              level="M"
              margin={1}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default InstagramWall;
