import React, { Component } from 'react';
import CountUp from 'react-countup';

import GooglQR from '../../utils/GoogleQR';

import './style.css';

class InstagramLikes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      instaLikes: 0,
      newInstaLikes: 0,
    };
    this.instaPostTimer = undefined;
  }

  componentDidMount() {
    this.startTimerInstaPost(
      {
        social: 'instagramlikes',
        profileId: 'BWqmnmWFokU',
        serviceToken: '87934974ffcce31fc8b4c276da851ccd306bf401',
      },
      1000 + Math.floor(Math.random() * 11),
    );
  }

  componentWillUnmount() {
    clearTimeout(this.instaPostTimer);
  }

  startTimerInstaPost(req, delay) {
    this.instaPostTimer = setTimeout(() => {
      this.updateSocialInfo(req);
    }, delay);
  }

  updateSocialInfo({ social, profileId, link, serviceToken }) {
    fetch(
      `https://boiling-brushlands-33834.herokuapp.com/api/v1/data/instagram/post?id=${profileId}&service_token=${serviceToken}&_=${new Date().getTime()}`,
      { cache: 'no-store' },
    )
      .then((response) => response.json())
      .then(
        (response) =>
          response.likes &&
          this.state.newInstaLikes !== response.likes &&
          this.setState((prevState) => ({
            instaLikes: prevState.newInstaLikes,
            newInstaLikes: response.likes,
          })),
      )
      .then(() =>
        this.startTimerInstaPost(
          { social, profileId, serviceToken },
          20000 + Math.floor(Math.random() * 11),
        ),
      )
      .catch((error) => {
        console.log(error);
        this.startTimerInstaPost(
          { social, profileId, serviceToken },
          20000 + Math.floor(Math.random() * 11),
        );
      });
  }

  render() {
    return (
      <div className="wrapper instagram_post">
        <div className="wrapper_inner">
          <h1 className="title">LIKE THIS PRODUCT</h1>
          <div className="countainers">
            <div className="profile">
              <i className="fa fa-heart" aria-hidden="true" />
            </div>
            <div className="followers">
              <div className="followers_count">
                <CountUp
                  start={this.state.instaLikes}
                  end={this.state.newInstaLikes}
                />
              </div>
            </div>
          </div>
          <div className="contact_us">
            <img
              className="contact_us_img"
              src="img/insta-text-logo.png"
              alt=""
            />
            <GooglQR
              url="https://www.instagram.com/p/BWqmnmWFokU/"
              style={{ width: '100%' }}
              level="M"
              margin={1}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default InstagramLikes;
