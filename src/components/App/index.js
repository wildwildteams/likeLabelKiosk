import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import ReactSlick from 'react-slick';
import InstagramWall from 'components/Instagram';
import TripAdvisor from 'components/TripAdvisor';
import InstagramLikes from 'components/InstagramLikes';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const slickSettings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
    };
    const styles = {
      position: 'absolute',
      left: 0,
      top: 0,
      width: '100%',
      height: '100%',
    };
    return (
      <div style={{ ...styles }}>
        <ReactSlick {...slickSettings}>
          <div>
            <InstagramWall />
          </div>
          <div>
            <TripAdvisor />
          </div>
          <div>
            <InstagramLikes />
          </div>
        </ReactSlick>
      </div>
    );
  }
}

export default hot(module)(App);
