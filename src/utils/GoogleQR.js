import React from 'react';
import PropTypes from 'prop-types';

const GooglQR = ({url, size, level, style, className, margin}) => {
  const path = `https://chart.googleapis.com/chart?cht=qr&chs=${size}x${size}&choe=UTF-8&chld=${level}|${margin}&chl=${url}`;

  return (
    <img src={path} style={style} className={className} alt="" />
  );
};

GooglQR.propTypes = {
  className: PropTypes.string,
  level: PropTypes.string,
  margin: PropTypes.number,
  size: PropTypes.number,
  style: PropTypes.object,
  url: PropTypes.string.isRequired
};

GooglQR.defaultProps = {
  level: 'H',
  size: 150,
  margin: 0
};

export default GooglQR;